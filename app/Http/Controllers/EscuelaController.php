<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\t_calificaciones;

class EscuelaController extends Controller
{
    //
    public function index($id_alumno=null)
    {
    	if($id_alumno && $id_alumno>0) //verifico que exista el valor y sea mayor a cero el id
    	{
    		$result=DB::table('t_alumnos')
    			->select('t_alumnos.id_t_usuarios','t_alumnos.nombre','t_alumnos.ap_paterno','t_materias.nombre','t_calificaciones.calificacion',DB::raw('date_format(t_calificaciones.fecha_registro,\'%d/%m/%Y\') as fecha'))
    			->join('t_calificaciones','t_alumnos.id_t_usuarios','=','t_calificaciones.id_t_usuarios')
    			->join('t_materias','t_calificaciones.id_t_materias','=','t_materias.id_t_materias')
    			->where('id_t_usuarios','=',$id_alumno)
    			->get();

    			return json_encode($result->rows());
    	}
    	else
    	{

    		$respuesta=['success'=>'error','msg'=>'No existe id para buscar'];
    	}

    	return json_encode($respuesta);
    }

    public function update(Request $request)
    {
    	$id_t_calificaciones=$request->$id_calificacion;
    	$calificacion=$request->calificacion;

    	$update=t_calificaciones::where('id_t_calificaciones','=',$id_t_calificaciones)
    			->update(['calificacion'=>$calificacion]);
    	if($update)
    	{
    		return json_encode(['success'=>'ok','msg'=>'calificacion actualizada']);
    	}
    	else
    	{
    		return json_encode(['success'=>'error','msg'=>'Error al actualizar calificacion']);
    	}

    }
    public function destroy($id_calificacion)
    {

    	$delete=t_calificaciones::where('id_t_calificaciones','=',$id_calificacion)
    			->delete();
    	if($delete)
    	{
    		return json_encode(['success'=>'ok','msg'=>'calificacion eliminada']);
    	}
    	else
    	{
    		return json_encode(['success'=>'error','msg'=>'Error al eliminar calificacion']);
    	}
    }

    public function store(Request $request)
    {
    	//obtengo los datos del request
    	$calificacion=$request->calif;
    	$materia_id=$request->id_materia;
    	$id_alumno=$request->id_alumno;

    	$insert=new t_calificaciones;
    	$insert->id_t_materias=$materia_id;
    	$insert->calificacion=$calificacion;
    	$insert->id_t_usuarios=$id_alumno;
    	$insert->fecha_registro=date('Y-m-d');
    	$insert->save();

    	if($insert)
    	{
    		return json_encode(['success'=>'ok','msg'=>'calificacion registrada']);
    	}
    	else
    	{
    		return json_encode(['success'=>'error','msg'=>'Error al insertar calificacion']);
    	}

    }
}
